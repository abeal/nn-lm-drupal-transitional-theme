#!/usr/bin/env bash
#Build script for CI server
echo=/bin/echo
$echo  "***********************";
$echo  "* build.sh            *";
$echo  "***********************";

$echo  "Building nnlm_drupal_theme Drupal module.";
$echo  "Testing for required executables..."

npm=$(which npm); # node required for grunt build system
if [ ! $npm ]; then
	$echo  "required executable node package manager (npm) not found.  cannot continue.";
	exit 1;
fi;

$echo "Installing grunt prerequisites..."
$npm install;

#project requires local composer install
$echo "Adding composer dependencies";
curl -sS https://getcomposer.org/installer | php
composer="/usr/bin/php "$(pwd)"/composer.phar";
$composer install;

$echo "Building software..."
grunt --no-color build;
