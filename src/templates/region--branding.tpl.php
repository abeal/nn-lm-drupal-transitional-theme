<div<?php
print $attributes; ?>>
<div<?php
print $content_attributes; ?>>
<div class="branding-data clearfix">
  <?php
  print $content; ?>
  <?php
  if ($site_name || $site_slogan): ?>
  <?php
  $class = $site_name_hidden && $site_slogan_hidden ? ' element-invisible' : ''; ?>
  <div id="banner">
    <div id="stylesheet_indicators"></div>
    <!-- for cross-browser grayscale transformations. -->
        <!--[CDATA[<svg xmlns="http://www.w3.org/2000/svg">
         <filter id="grayscale">
          <feColorMatrix type="matrix" values="0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0"/>
         </filter>
       </svg>-->
       <!--<img id="logo" src="/sites/all/themes/nnlm/img/nnlm_logo_100p.gif" alt="NN/LM Logo" />-->
       <?php
//determine assigned editorial section
//TODO: move this to the theming module
       $site_name = 'National Network of';
       $site_slogan = 'Libraries <span class="site-slogan-divider">of</span> Medicine';
       if(preg_match("/ws-sandy-dev/", $_SERVER['HTTP_HOST'])){
        $site_slogan .= "<span class='server-info-block'> Development Server<br />(next overwrite: <span id='overwrite-timer'></span>)</span>";
      }
      else if(preg_match("/ws-sandy-stage/", $_SERVER['HTTP_HOST'])){
        $site_slogan .= "<span class='server-info-block'> Staging Server<br />(next overwrite: <span id='overwrite-timer'></span>)</span>";
      }
      $as_sitesearch_url = '/search-results';
      $site_section = array_shift(explode('/', request_path()));
      $section = array_shift(taxonomy_get_term_by_name($site_section, 'sections'));
      if(!$section){
        $site_section = 'national';
        $section = array_shift(taxonomy_get_term_by_name('national', 'sections'));
      }
      else{
        $section_name = $section->field_full_section_name[LANGUAGE_NONE][0]['value'];
        $as_sitesearch_url = '/'.$site_section.'/search-results';
      }

      ?>
      <div class="site-name-logotype<?php
      print $class; ?>">
      <object id="nnlm_logo" data="/sites/all/themes/nnlm/img/nnlm_logo.svg" type="image/svg+xml"></object>
      <?php
      if ($site_name): ?>
      <?php
      $class = $site_name_hidden ? ' element-invisible' : ''; ?>
      <p class="site-name-logotype-top<?php
      print $class; ?>"><?php
      print $site_name; ?></p>
      <?php
      endif; ?>
      <?php
      if ($site_slogan): ?>
      <?php
      $class = $site_slogan_hidden ? ' element-invisible' : ''; ?>
      <p class="site-name-logotype-bottom<?php
      print $class; ?>"><?php
      print $site_slogan;?>
    </p>
    <?php
    endif; ?>
  </div>
  <div class="site-name-editorial-section"><p><a href="/<?php print @$site_section; ?>"><?php print @$section_name; ?></a></p></div>

  <div class='site-search'>
      <!--Results url: <?php print $as_sitesearch_url; ?> -->
      <form accept-charset="utf-8" action="<?php print @$as_sitesearch_url; ?>">
        <table cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td >
                <input autocomplete="off" type="text" size="10" name="google" title="search" x-webkit-speech="" x-webkit-grammar="builtin:search" lang="en" dir="ltr" spellcheck="false">
              </td>
              <td >
                <input type="submit" value="Search" title="search">
              </td>
            </tr>
          </tbody>
        </table>
      </form>
  </div>
  <?php
  endif; ?>
</div>
</div>
</div>
</div>
