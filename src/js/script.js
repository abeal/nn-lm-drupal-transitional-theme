(function($) {
  Drupal.behaviors.NNLM = Drupal.behaviors.NNLM || {};
  Drupal.behaviors.NNLM.version_number = "1.0.0";
  Drupal.behaviors.NNLM.theme = {
    /**
     * Invoked on page load by nnlm_core module's js
     */
    attach: function(context, settings) {
      this.dump("********************************\nNN/LM theme javascript initialized\n********************************");
      //this.style_navigational_menu(context);
      this.bind_google_cse(context);
      this.back_to_top_buttons(context);
      this.set_webfonts(context);
      this.replace_svg_files_with_inline_equivalents(context);
      this.animate_map(context);
      //dev only
      var next_scheduled = new Date();
      if (window.location.href.indexOf("drupal.dev") > -1) {
        jQuery.getScript('script-dev.js', function(){
          console.log("development scripts loaded");
        });
      }
      else if (window.location.href.indexOf("sandy-dev") > -1) {
        console.log("Development environment detected");
        var days_until_this_saturday = Math.abs(next_scheduled.getDay() - 6);
        //console.log("Days until saturday: " + days_until_this_saturday);
        next_scheduled.setDate(next_scheduled.getDate() + days_until_this_saturday);
        next_scheduled.setHours(0);
        next_scheduled.setMinutes(0);
        next_scheduled.setSeconds(0);
        $("#overwrite-timer").text(next_scheduled.toLocaleString());
      }
      else if (window.location.href.indexOf("sandy-stage") > -1){
        console.log("Staging environment detected");
        next_scheduled.setDate(next_scheduled.getDate() + 1); //tomorrow at 12AM
        next_scheduled.setHours(0);
        next_scheduled.setMinutes(0);
        next_scheduled.setSeconds(0);
        console.log("Next Scheduled: " + next_scheduled);
        $("#overwrite-timer").text(next_scheduled.toLocaleString());
      }
      $.each(Drupal.behaviors.NNLM.theme.callbacks, function(fn) {
        fn();
      });
    },
    callbacks: {},
    bind_google_cse: function(context){
      //@deprecated: implementation moved to nnlm_google_cse module.
    },
    animate_map: function(context) {
      var map = $("#nnlm_map svg");
      if (!map) {
        return;
      }
      console.log("Animating map");
    },
    dump: function(msg) {
      Drupal.behaviors.NNLM.core.dump(msg);
    },
    replace_svg_files_with_inline_equivalents: function(context) {
      jQuery('img.svg').each(function() {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
        jQuery.get(imgURL, function(data) {
          // Get the SVG tag, ignore the rest
          var $svg = jQuery(data).find('svg');

          // Add replaced image's ID to the new SVG
          if (typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
          }
          // Add replaced image's classes to the new SVG
          if (typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass + ' replaced-svg');
          }

          // Remove any invalid XML tags as per http://validator.w3.org
          $svg = $svg.removeAttr('xmlns:a');

          // Replace image with new SVG
          $img.replaceWith($svg);
        }, 'xml');
      });
    },
    set_webfonts: function(context) {
      if (typeof WebFont === 'undefined') {
        this.dump("Skipping custom fonts - Google WebFonts loader not available");
        return;
      }
      var web_font_list = [
        'Roboto',
        'Cinzel'
      ];
      this.dump("binding " + web_font_list[0] + " webfont");
      WebFont.load({
        google: {
          families: web_font_list
        }
      });
    },
    back_to_top_buttons: function(context) {
      var duration = 500;
      jQuery('.back-to-top,.toc-back-to-top a').click(function(event) {
        event.preventDefault();
        jQuery('html, body').animate({
          scrollTop: 0
        }, duration);
        return false;
      });
    }
  };
})(jQuery);
console.log("NN/LM Theme script.js loaded");