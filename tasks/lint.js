module.exports = function(grunt) {
	grunt.registerTask("lint", [
		'scsslint:default', //syntax check sass and scss files
		'jshint:default', //syntax check js
		'phplint:default' //syntax check servlet files
	]);
};