module.exports = function(grunt, options) {
  var semver = require('semver');
  var currentVersion = options.package.version;
  var output = {
    bump: {
      options: {
        questions: [{
          config: 'bump.prompt.increment',
          type: 'list',
          message: 'Bump version from ' + currentVersion + ' to:',
          choices: [{
            value: 'build',
            name: 'Build:  ' + (currentVersion + '-?') + ' Unstable, betas, and release candidates.'
          }, {
            value: 'patch',
            name: 'Patch:  ' + semver.inc(currentVersion, 'patch') + ' Backwards-compatible bug fixes.'
          }, {
            value: 'minor',
            name: 'Minor:  ' + semver.inc(currentVersion, 'minor') + ' Add functionality in a backwards-compatible manner.'
          }, {
            value: 'major',
            name: 'Major:  ' + semver.inc(currentVersion, 'major') + ' Incompatible API changes.'
          }],
          'default': 'patch'
        }, {
          config: 'bump.prompt.commit',
          type: 'confirm',
          message: 'Commit?',
          'default': false
        }, {
          config: 'bump.prompt.commitMessage',
          type: 'input',
          'when': function(answers) {
            return answers['bump.prompt.commit'];
          },
          message: 'Commit Message:',
          'default': 'Release v%VERSION%'
        }, {
          config: 'bump.prompt.createTag',
          type: 'confirm',
          message: 'Tag version in git?',
          'default': false
        }, {
          config: 'bump.prompt.tagName',
          type: 'input',
          'when': function(answers) {
            return answers['bump.prompt.createTag'];
          },
          message: 'Tag Name:',
          'default': 'v%VERSION%'
        }, {
          config: 'bump.prompt.tagMessage',
          type: 'input',
          'when': function(answers) {
            return answers['bump.prompt.createTag'];
          },
          message: 'Tag Message:',
          'default': 'Version %VERSION%'
        }],
        then: function(results) {
          grunt.task.run([
            'bump:' + results['bump.prompt.increment']
          ]);
          
        }
      }
    }
  };
  return output;
};