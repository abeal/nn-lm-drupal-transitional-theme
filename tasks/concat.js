module.exports = function(grunt) {
	var config = {
		js: {
			options: {
				separator: "\n",
				sourceMap: false, //@see http://www.html5rocks.com/en/tutorials/developertools/sourcemaps/
				stripBanners: true, //removes comments
      	nonull: true
			},
			files: {},
			nonull: true
		},
		css: {
			options: {
				separator: "\n",
				nonull: true,
				sourceMap: false
			},
			files: {}
		}
	};
	config.js.files['build/js/<%=javascript_filename %>'] = [
		'src/js/script.js'
	];

	//build subthemes
	var sections = [
		'archive',
		'evaluation',
		'gmr',
		'mar',
		'mcr',
		'national',
		'ner',
		'pnr',
		'psr',
		'scr',
		'sea'
	];
	for (var i in sections) {
		var section = sections[i];
		config.css.files['build/css/' + section + '-global.css'] = [
			'src/css/' + section + '/global/*.css'
		];
		config.css.files['build/css/' + section + '-alpha-default.css'] = [
			'src/css/' + section + '/default/*.css'
		];
		config.css.files['build/css/' + section + '-alpha-default-narrow.css'] = [
			'src/css/' + section + '/narrow/*.css'
		];
		config.css.files['build/css/' + section + '-alpha-default-normal.css'] = [
			'src/css/' + section + '/normal/*.css'
		];
		config.css.files['build/css/' + section + '-alpha-default-wide.css'] = [
			'src/css/' + section + '/wide/*.css'
		];
	}
	return config;
};