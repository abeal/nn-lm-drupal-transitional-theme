module.exports = function(grunt) {
    grunt.registerTask("sandy-dev", [
        'build',
        'rsync:dev'
    ]);
    grunt.registerTask("sandy-stage", [
        'build',
        'rsync:stage'
    ]);
    return {
        options: {
            args: ["--verbose"],
            exclude: [".git*", "*.scss", "node_modules"],
            compareMode: 'checksum',
            recursive: true
        },
        dev: {
            options: {
                src: "build/",
                dest: "/var/www/ws-sandy-dev.hsl.washington.edu/sites/all/themes/nnlm",
                host: 'abeal@november.hsl.washington.edu',
                //dryRun:true,
                deleteAll: true // Careful this option could cause data loss, read the docs!
            }
        },
        stage: {
            options: {
                src: "build/",
                dest: "/var/www/ws-sandy-stage.hsl.washington.edu/sites/all/themes/nnlm",
                host: 'abeal@november.hsl.washington.edu',
                //dryRun:true,
                deleteAll: true // Careful this option could cause data loss, read the docs!
            }
        }
        //note: stage and prod are accomplished by git commits (CI server)
    };
};