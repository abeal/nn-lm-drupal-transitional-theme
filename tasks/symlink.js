module.exports = {
  // Enable overwrite to delete symlinks before recreating them
  options: {
    overwrite: true
  },
  // The "build/target.txt" symlink will be created and linked to
  // "source/target.txt". It should appear like this in a file listing:
  // build/target.txt -> ../source/target.txt
  build: {
    src: 'build/js/ckeditor.config.js',
    dest: 'build/ckeditor.config.js'
  }
};