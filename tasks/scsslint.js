//https://www.npmjs.org/package/grunt-scss-lint
module.exports = {
		options: {
			config: '.scss-lint.yml'
		},
		'default': [
			'src/sass/**/*.scss'
		]
};